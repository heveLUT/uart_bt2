#include <msp430g2553.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "uart.h"

/*
 * main.c
 */

/* Readable pin defines and other constants */
#define RLED 	BIT0
#define GLED	BIT6
#define RX		BIT1
#define TX		BIT2


/* FUNCTION DECLARATIONS */
void board_setup(void);
void delay_cycles(uint32_t i);
int arcmp(unsigned char *buf, unsigned char *word, int len);

// UART Port Configuration parameters and registers
UARTConfig cnf;
USCIUARTRegs uartUsciRegs;
USARTUARTRegs uartUsartRegs;

// Buffers to be used by UART Driver
unsigned char uartTxBuf[50];
unsigned char uartRxBuf[50];


/* GLOBAL VARIABLES */



/*
 * main.c
 */

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    __disable_interrupt();
    board_setup();
    __enable_interrupt();
    int bytesAvailable = 0;
    uint32_t delayCycles = 100000;


    for(;;) {

    	delay_cycles(delayCycles);
    	/*
    	delay_cycles(delayCycles);
    	P1OUT ^= GLED;
    	bytesAvailable = numUartBytesReceived(&cnf);
    	if(bytesAvailable > 0) {
    		unsigned char tempBuf[10];
    		memset(tempBuf,0,10);

    		volatile int bytesRead = readRxBytes(&cnf, tempBuf, 4, 0);

    		if(arcmp(tempBuf, (unsigned char *)"test", 4) == 0) {

				P1OUT ^= RLED;


    			uartSendDataInt(&cnf, (unsigned char *)"sanaOK\n", strlen("sanaOK\n"));
    		} else if(tempBuf[0] == 'a') {
    			P1OUT ^= RLED;
    		}
    		else {
    			uartSendDataInt(&cnf, (unsigned char *)"OK\n", strlen("OK\n"));
    			//uartSendDataInt(&cnf, tempBuf, 10);
    		}
    	}


/*
		delay_cycles(delayCycles);

    	int bytesAvailable = numUartBytesReceived(&cnf);
    	if(bytesAvailable > 0) {
    		unsigned char tempBuf[20];
    		memset(tempBuf,0,20);

    		volatile int bytesRead = readRxBytes(&cnf, tempBuf, bytesAvailable, 0);

    		if(bytesRead == bytesAvailable) {
    			uartSendDataInt(&cnf, &tempBuf[0], bytesAvailable);

    			if(tempBuf[0] == 'a') {
    				P1OUT ^= GLED;
    				uartSendDataInt(&cnf, (unsigned char *)"aOK\r\n", strlen("aOK\r\n"));
    			}

    		} else {
    			__no_operation();
    		}
    	}

*/
    }


	return 0;
}

/* Compares starts of arrays for len bytes, returnes 0 if equal, -1 if not*/
int arcmp(unsigned char *buf, unsigned char *word, int len) {
	int idx;
	for(idx = 0; idx < len; idx++) {
		if(buf[idx] != word[idx]) {
			return -1;
		}
	}
	return 0;
}

/* Pin and other setup at start */
void board_setup(void) {

	DCOCTL = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;			// Calibrate to 1Mhz clock

	P1OUT = 0;
	P1DIR |= RLED + GLED;

	P1SEL |= RX + TX;				// Config RX and TX pins for UART
	P1SEL2 |= RX + TX;

	initUartDriver();

	// Configure UART Module on USCIA0
	cnf.moduleName = USCI_A0;

	// Use UART Pins P1.1 and P1.2
	cnf.portNum = PORT_1;
	cnf.RxPinNum = PIN1;
	cnf.TxPinNum = PIN2;

	// 115200 Baud from 8MHz SMCLK
	cnf.clkRate = 1000000L;
	cnf.baudRate = 9600L;
	cnf.clkSrc = UART_CLK_SRC_SMCLK;

	// 8N1
	cnf.databits = 8;
	cnf.parity = UART_PARITY_NONE;
	cnf.stopbits = 1;

	int res = configUSCIUart(&cnf,&uartUsciRegs);
	if(res != UART_SUCCESS)
	{
		// Failed to initialize UART for some reason
		__no_operation();
	}

	// Configure the buffers that will be used by the UART Driver.
	// These buffers are exclusively for the UART driver's use and should not be touched
	// by the application itself. Note that they may affect performance if they're too
	// small.
	setUartTxBuffer(&cnf, uartTxBuf, 50);
	setUartRxBuffer(&cnf, uartRxBuf, 50);

	enableUartRx(&cnf);

}

/* Delay i cycles */
void delay_cycles(volatile uint32_t i) {
	do i--;
	while(i != 0);
}
